#ifndef GLOBAL_H_
#define GLOBAL_H_

#define F_CPU 8000000L 
#define BAUD 31250					//MIDI baud rate standard
#define UBBR_VALUE F_CPU/16/BAUD-1	//defines BAUD in AVR (leastways in ATmega8)

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>

volatile char disrupt_flag;			// start time counting | 0 or 1

/* 
 * STRING_INT informs about one of 13 laser beams disruption.
 * It's digital sygnal that produces interrupt <name> in uC by down slope (check it out).
 * A total rapture makes LOW state on respective STRING.
 * The time between STRING_INT and STRINGX event gives velocity information what is relevant with sound volume in MIDI standard.
 */

#define NUMBER_OF_SCALES 12
#define NUMBER_OF_STRINGS 13

#define DISRUPT_MAX_TIME_MS 30				// [ms]
#define BREAK_1_2(x) !(PINC & (1<<x))	// PINC && (1<<x)
#define BREAK_3_7(x) !(PIND & (1<<x))
#define BREAK_8_13(x) !(PINB & (1<<x))

#define STRING_INT INT0
#define STRING1 PC4
#define STRING2 PC5
#define STRING3 PD3
#define STRING4 PD4
#define STRING5 PD5
#define STRING6 PD6
#define STRING7 PD7
#define STRING8 PB0
#define STRING9 PB1
#define STRING10 PB2
#define STRING11 PB3
#define STRING12 PB4
#define STRING13 PB5

//#define FOOT ADC0					/* 127 different values footswitch as ADC	*/
#define FOOT PC0					/* binary footswitch as normal I/O			*/
#define KNOB_TONE PC1				// ADC1: knob1
#define KNOB_SCALE PC2				// ADC2: knob2
#define BUTTON PC3			// front button -> octave or MODE changer
#define THER1 PC3					// ADC6: theremin vertical -> pitchband?
#define THER2 PC4					// ADC7: theremin horizontal -> reverb or other CC?

#endif /* GLOBAL_H_ */