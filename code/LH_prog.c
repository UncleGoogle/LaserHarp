/*
 * Laser Harp 
 * KNMiA and KNF cooperation
 *
 * Created: 2015-04-30 22:51:30
 *  Author: Mieszko
 */

/* UM-ONE (midi-usb interface) troubleshooting
 * When disconnect usb during transmission: 
 * 1 reconnect usb
 * 2 close and open program that use UM-ONE
 */
/************************************************************************/
/*								POSTEPY                                 */
/************************************************************************/
/* 25.05 Bug do znalezienia: niezaleznie jednoczenie grana nuta G6      */
/* 28.06 zakomentowanie przerwa� USART gdy przychodzi dana - spr. czy ok*/
/* moze warto sprawdzic ADC (jesli nie bedzie chodzic) pod katem		*/
/* dodania preskalera - np 32                                           */
/************************************************************************/
/************************************************************************/

#include <stdio.h>
#include "global.h"
#include "lcd_lib.h"
#include "usart.h"
#include "midi.h"
#include "setup.h"

midi_sound string[NUMBER_OF_STRINGS];

static int lcd(char c, FILE *stream){			/* LCD printf							*/
	lcd_putc(c);
	return 0;
}
static FILE mystdout = FDEV_SETUP_STREAM(lcd, NULL, _FDEV_SETUP_WRITE);

ISR (USART_UDRE_vect){
	send_midi();
}
ISR (INT0_vect){	
	//disrupt_flag = 1;
	//TCNT0 = 0x00;								/* Counter value						*/
	//TIMSK |= (1<<TOIE0);						/* Timer0 Overflow Interrupt Enable		*/
}
ISR (TIMER0_OVF_vect){
	disrupt_flag = 0;
	TIMSK &= ~(1<<TOIE0);						/* Timer0 Overflow Interrupt Disable	*/
}
ISR (TIMER1_COMPA_vect){
	sei();
	midi_package ton_ = tonality;
	midi_package sca_ = scale;
	ADMUX |=(1<<MUX0);							/*																							*/
	ADMUX &=~(1<<MUX1);							/*		ADC1 (TONE_KNOB) enable																*/
	ADCSRA |= (1<<ADSC);						/* Start single conversion. It will be cleared by hardware when the conversion is completed */
	while (ADCSRA & (1 << ADSC));				/* Wait until conversion is completed														*/
	tonality = DEFAULT_TONALITY + ADCH*0.0454;	/* ADCH has 8 bits. 256/12 = 21,(3); to be sure: 1/22 = 0.0(45)								*/
	//test_value_lcd(0,0,"ADCH=",ADCH);
	//test_value_lcd(8,0,"t",tonality);
	ADMUX &=~(1<<MUX0);							/*																							*/
	ADMUX |=(1<<MUX1);							/*		ADC2 (SCALE_KNOB) enable															*/
	ADCSRA |= (1<<ADSC);						/* Start single conversion. It will be cleared by hardware when the conversion is completed */
	while (ADCSRA & (1 << ADSC));				/* Wait until conversion is completed														*/
	scale = ADCH*0.0454;						/* ADCH has 8 bits. 256/12 = 21,(3); to be sure: 1/22 = 0.0(45)								*/
	//test_value_lcd(0,1,"ADCH=",ADCH);
	//test_value_lcd(8,1,"s",tonality);
	//set_keys();									// sets new values always
	//if (!((ton_==tonality)&&(sca_==scale)))	// if any state change has occured // krzaki. zbadac problem
	//{
		//lcd_clrscr();
		//test_value_lcd(0,0,"tonality =",tonality);
		//test_value_lcd(0,1,"scale =",scale);
		for (int i=0; i<13;i++){				// send noteOff's when notes have been changing
			noteOff(&string[i]);
			string[i].noteOn_flag = 1;			// cause they are probably still breaking. 
		}
		set_keys();								// sets new values
	//}
}
ISR (TIMER2_OVF_vect){
	// for what?
}

int main(void){
	
	disrupt_flag = 0;
	//DEFINE STRINGS and ADCs AS INPUTS
	DDRC &= ~(1<<STRING_INT)&~(1<<STRING1)&~(1<<STRING2)&~(1<<KNOB_TONE)&~(1<<KNOB_SCALE)&~(1<<BUTTON);
	DDRD &= ~(1<<STRING3)&~(1<<STRING4)&~(1<<STRING5)&~(1<<STRING6)&~(1<<STRING7);
	DDRB &= ~(1<<STRING8)&~(1<<STRING9)&~(1<<STRING10)&~(1<<STRING11)&~(1<<STRING12)&~(1<<STRING13);
	//ACTIVATE pull-up resistors
	PORTC |= (1<<STRING_INT)|(1<<STRING1)|(1<<STRING2)|(1<<KNOB_TONE)|(1<<KNOB_SCALE)|(1<<BUTTON);
	PORTD |= (1<<STRING3)|(1<<STRING4)|(1<<STRING5)|(1<<STRING6)|(1<<STRING7);
	PORTB |= (1<<STRING8)|(1<<STRING9)|(1<<STRING10)|(1<<STRING11)|(1<<STRING12)|(1<<STRING13);
	

	ADMUX |=(1<<REFS0)|(1<<ADLAR);				/* ADLAR - left adjust result			*/
	ADMUX &=~(1<<REFS1);						/* V_REF = AVcc							*/
	ADCSRA |=(1<<ADEN);							/* ADC Enable							*/

	//GICR |= (1<<INT0);							/* INT0 enable							*/
	MCUCR &= ~(1<<ISC00);						/* The falling edge of INT0 generates	*/
	MCUCR |= (1<<ISC01);						/*				 an interrupt request	*/

	TCCR0 &= ~(1<<CS01);						/* TC0 8bit  	preskaler: 1/1024		*/
	TCCR0 |= (1<<CS00)|(1<<CS02);				/*				overflow time is ~33ms	*/
	
	TCCR1B |= (1<<CS12);						/* TC1 16bit	preskaler: 1/256		*/
	TCCR1B &= ~(1<<CS10)&~(1<<CS11);			/*				overflow time is ~2097ms*/
	TCCR1A &= ~(1<<WGM10);						/* CTC1 mode							*/
	TCCR1A &= ~(1<<WGM11);						/*										*/
	TCCR1B |= (1<<WGM12);						/*										*/
	TCCR1B &= ~(1<<WGM13);						/*										*/
	OCR1A =	ADC_DELAY_MS*F_CPU/256;				/*										*/
	//TIMSK |= (1<<OCIE1A);						/* ADC checker compare interrupt enable */
	
	TCCR2 |=(1<<CS22)|(1<<CS21)|(1<<CS20);		/* TC2 8bit  	preskaler: 1/1024		*/
	TIMSK |= (1<<TOIE2);					

	init();										/* inter alia string[] initialization	*/
	
	//stdout = &mystdout;							/* LCD									*/
	//_delay_ms(1000);							/* wait for LCD							*/
	//lcd_init(LCD_DISP_ON_CURSOR_BLINK);
	
	
    while(1){	
		
		/* beam_break | noteOn_flag | required noteAction |
				1			0				1				// beam just broken			-> send noteOn
				1			1				0				// beam still breaking		-> do nothing
				0			1				1				// beam just unbroken		-> send noteOff
				0			0				0				// beam still not breaking	-> do nothing
		so thats why this is XOR (^) in conditions below*/
		
		if((BREAK_1_2(STRING1))^(string[0].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[0]);
			noteAction(&string[0]);
		}
		if((BREAK_1_2(STRING2))^(string[1].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[1]);
			noteAction(&string[1]);
		}
		if((BREAK_3_7(STRING3))^(string[2].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[2]);
			noteAction(&string[2]);
		}
		if((BREAK_3_7(STRING4))^(string[3].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[3]);
			noteAction(&string[3]);
		}
		if((BREAK_3_7(STRING5))^(string[4].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[4]);
			noteAction(&string[4]);
		}
		if((BREAK_3_7(STRING6))^(string[5].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[5]);
			noteAction(&string[5]);
		}
		if((BREAK_3_7(STRING7))^(string[6].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[6]);
			noteAction(&string[6]);
		}
		if((BREAK_8_13(STRING8))^(string[7].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[7]);
			noteAction(&string[7]);
		}
		if((BREAK_8_13(STRING9))^(string[8].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[8]);
			noteAction(&string[8]);
		}
		if((BREAK_8_13(STRING10))^(string[9].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[9]);
			noteAction(&string[9]);
		}
		if((BREAK_8_13(STRING11))^(string[10].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[10]);
			noteAction(&string[10]);
		}
		if((BREAK_8_13(STRING12))^(string[11].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[11]);
			noteAction(&string[11]);
		}
		if((BREAK_8_13(STRING13))^(string[12].noteOn_flag)){
			//if (disrupt_flag)
				//set_velocity(&string[12]);
			noteAction(&string[12]);
		}
    }
	return 1;
}

