#include <stdio.h>
#include "setup.h"
#include "global.h"
#include "usart.h"
#include "lcd_lib.h"


void init(void){
	usart_init(UBBR_VALUE);
	for(int i=0;i<13;i++){
		string[i].channel=DEFAULT_CHANNEL;
		string[i].note=DEFAULT_TONALITY+interval[DEFAULT_SCALE][i];
		string[i].velocity=DEFAULT_VELOCITY;
		string[i].noteOn_flag=0;
	}	
	sei();					/* SREG |= (1<<I) ->interrupts enable	*/
}

void set_keys(void){
	for(int i=0;i<13;i++)
		string[i].note=tonality+interval[scale][i];							
}

void set_velocity(midi_sound *sound){
	sound->velocity = 127-(TCNT0/2);		// 127 - max volume
	disrupt_flag = 0;
}

void test_value_lcd(unsigned char x, unsigned char y, const char *string, const unsigned char val){
	lcd_clrscr();
	lcd_gotoxy(x,y);
	printf("%s %d",string,val);
	_delay_ms(TEST_DELAY_MS);
}