#include "usart.h"


void usart_init( unsigned int baud )
{
	UBRRH = (unsigned char)(baud>>8);
	UBRRL = (unsigned char)baud;						/* baud - prędkość transmisji						*/		
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);			/* Format ramki: słowo=8bitów, 2 bity stopu			*/
	UCSRB = (1<<RXEN)|(1<<TXEN);						/* Włączenie odbiornika i nadajnika					*/
	//UCSRB |= (1<<RXCIE);								/* Włączenie przerwania, gdy przyszła dana			*/
	UCSRB |= (1<<UDRIE);								/* Włączenie przerwania, gdy bufor nadawczy pusty	*/
}

