#ifndef SETUP_H_
#define SETUP_H_

#include "midi.h"

#define TEST_DELAY_MS 300
#define TEST_NOTES_DELAY_MS 100

#define ADC_DELAY_MS  500		// max 2097 for 256 preskaler on 16bit TC1

#define DEFAULT_CHANNEL		0
#define DEFAULT_VELOCITY	80
#define DEFAULT_SCALE		SCALE_BLUES_MAJOR
#define DEFAULT_TONALITY	C4

extern midi_sound string[NUMBER_OF_STRINGS];

void init(void);
void set_keys(void);
void set_velocity(midi_sound *sound);

void test_value_lcd(unsigned char x, unsigned char y, const char *string, const unsigned char val);
#endif /* SETUP_H_ */