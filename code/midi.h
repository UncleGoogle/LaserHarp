#ifndef MIDI_H_
#define MIDI_H_

#include "global.h"

// MIDI config
#define MIDI_CTRL_START_NUM			24

// midi events
#define MIDI_NOTE_OFF			0x80
#define MIDI_NOTE_ON			0x90
#define MIDI_POLY_TOUCH 	0xA0
#define MIDI_CONTROL_CHANGE	0xB0
#define MIDI_PROGRAM_CHANGE	0xC0
#define MIDI_CHANNEL_TOUCH	0xD0
#define MIDI_PITCH_BEND			0xE0

// midi system events
#define MIDI_SYSEX_START        0xF0
#define MIDI_SYSEX_END          0xF7
#define MIDI_MTC                0xF1
#define MIDI_SPP                0xF2
#define MIDI_SONG_SEL           0xF3
#define MIDI_TUNE_REQ           0xF6
#define MIDI_CLOCK              0xF8
#define MIDI_SYNC               0xF9
#define MIDI_START              0xFA
#define MIDI_STOP               0xFC
#define MIDI_CONT               0xFB
#define MIDI_SENSE              0xFE
#define MIDI_RESET              0xFF

#define MIDI_DATA_MASK			0x7F
#define MIDI_STATUS_MASK		0xF0
#define MIDI_CHANNEL_MASK		0x0F

// MIDI specific macros
#define MIDI_MAKE_CMD(cmd, chanel)	(BYTE)(cmd | (chanel & MIDI_CHANNEL_MASK))
#define MIDI_IS_CMD(byte)			(bool)(byte & 0x80)
#define MIDI_IS_DATA(byte)			!((bool)(byte & 0x80))

/* scales */
#define SCALE_CHROMATIC			0
#define SCALE_IONIAN			1
#define SCALE_LYDIAN			2
#define SCALE_MIXOLYDIAN		3
#define SCALE_DORIAN			4
#define SCALE_AEOLIAN			5
#define SCALE_PHRYGIAN			6
#define SCALE_LOCRIAN			7
#define SCALE_HARMONIC_MINOR	8
#define SCALE_DIMINISHED		9
#define SCALE_BLUES_MINOR		10
#define SCALE_BLUES_MAJOR		11

enum tone {	C_1, Cis_1, D_1, Dis_1, E_1, F_1, Fis_1, G_1, Gis_1, A_1, Ais_1, B_1,
						C0, Cis0, D0, Dis0, E0, F0, Fis0, G0, Gis0, A0, Ais0, B0,
						C1, Cis1, D1, Dis1, E1, F1, Fis1, G1, Gis1, A1, Ais1, B1,
						C2, Cis2, D2, Dis2, E2, F2, Fis2, G2, Gis2, A2, Ais2, B2,
						C3, Cis3, D3, Dis3, E3, F3, Fis3, G3, Gis3, A3, Ais3, B3,
						C4,	Cis4, D4, Dis4, E4, F4, Fis4, G4, Gis4, A4, Ais4, B4,
						C5,	Cis5, D5, Dis5, E5, F5, Fis5, G5, Gis5, A5, Ais5, B5,
						C6, Cis6, D6, Dis6, E6, F6, Fis6, G6, Gis6, A6, Ais6, B6,
						C7, Cis7, D7, Dis7, E7, F7, Fis7, G7, Gis7, A7, Ais7, B7,
						C8, Cis8, D8, Dis8, E8, F8, Fis8, G8, Gis8, A8, Ais8, B8,
						};
typedef enum tone tone;

struct midi_sound{
	unsigned char channel;
	tone note					: 7;		/* bit poles to save memory space */
	unsigned char velocity;
	unsigned char noteOn_flag	: 1;
};
typedef struct midi_sound midi_sound;

void test_note(midi_sound *sound);
void noteOn (midi_sound *sound);
void noteOff (midi_sound *sound);
void noteAction (midi_sound *sound);
void send_midi(void);

typedef volatile unsigned char midi_package;		//dla zmylenia przeciwnika

midi_package TXBuffer[3];		//USART sending bufor
midi_package interval[NUMBER_OF_SCALES][NUMBER_OF_STRINGS];
midi_package tonality;
midi_package scale;


#endif /* MIDI_H_ */