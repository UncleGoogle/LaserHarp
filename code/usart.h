#ifndef USART_H_
#define USART_H_
#include <avr/interrupt.h>
#include <stdlib.h>

void usart_init( unsigned int baud );

#endif
