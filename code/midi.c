#include "midi.h"
#include "usart.h"
#include "global.h"
#include "lcd_lib.h"
#include "setup.h"

midi_package tonality = DEFAULT_TONALITY;
midi_package scale = DEFAULT_SCALE;
midi_package interval[NUMBER_OF_SCALES][NUMBER_OF_STRINGS] = {
	/*  String number				 0|1|2|3|4|5| 6| 7| 8| 9| 10|11|12| */
	/* 0. Chromatic			*/		{0,1,2,3,4,5, 6, 7, 8, 9, 10,11,12},
	/* 1. Ionian (Major)	*/		{0,2,4,5,7,9, 11,12,14,16,17,19,21},
	/* 2. Lydian	    	*/		{0,2,4,6,7,9, 11,12,14,16,18,19,21},
	/* 3. Mixolydian		*/		{0,2,4,5,7,9, 10,12,14,16,17,19,21},
	/* 4. Dorian 			*/		{0,2,3,5,7,9, 10,12,14,15,17,19,21},
	/* 5. Aeolian 			*/		{0,2,3,5,7,8, 10,12,14,15,17,19,20},
	/* 6. Phrygian			*/		{0,1,3,5,7,8, 10,12,13,15,17,19,20},
	/* 7. Locrian			*/		{0,1,3,5,6,8, 10,12,13,15,17,18,20},
	/* 8. Harmonic Minor	*/		{0,2,3,5,7,8, 11,12,14,15,17,19,20},
	/* 9. Diminished		*/		{0,2,3,5,6,8, 9, 11,12,14,15,16,18},
	/* 10. Blues Minor		*/		{0,3,5,6,7,10,12,15,17,18,19,22,24},
	/* 11. Blues Major		*/		{0,2,3,4,7,9, 12,14,15,16,19,21,24}};

// Debugging Mode
void test_note(midi_sound *sound){ 
	lcd_gotoxy(0,sound->noteOn_flag); // y=1 -> noteOn; y=0 -> noteOff
	printf("Note_%d 1On;0Off",sound->note);
	_delay_ms(TEST_NOTES_DELAY_MS);
	lcd_clrscr();
}

void noteOn (midi_sound *sound){
	TXBuffer[0] = MIDI_NOTE_ON | (MIDI_CHANNEL_MASK & sound->channel);
	TXBuffer[1] = MIDI_DATA_MASK & sound->note;
	TXBuffer[2] = MIDI_DATA_MASK & sound->velocity;
	UCSRB |= (1<<UDRIE);
	sound->noteOn_flag = 1;
}

void noteOff (midi_sound *sound){
	TXBuffer[0] = MIDI_NOTE_OFF | (MIDI_CHANNEL_MASK & sound->channel);
	TXBuffer[1] = MIDI_DATA_MASK & sound->note;
	TXBuffer[2] = MIDI_DATA_MASK & sound->velocity;
	UCSRB |= (1<<UDRIE);
	sound->noteOn_flag = 0;
}

void noteAction (midi_sound *sound){
	if(sound->noteOn_flag)
		noteOff(sound);
	else noteOn(sound);
	//test_note(sound); // Debugging Mode
}

void send_midi(void){
	static int txindex=0;
	
	if (TXBuffer[txindex]){			//jesli jest znak w buforze
		UDR = TXBuffer[txindex++];	//to wyslij i przesun wskaznik na kolejny indeks tablicy
	}
	else {
		txindex=0;
		UCSRB &= ~(1<<UDRIE);		//wylacz przerwania| wlaczanie w midiOn & midiOff
	}
}